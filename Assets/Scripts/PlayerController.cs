using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public Animator anim;
    public CharacterController controller;
    public float forwardSpeed;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Horizontal")!=0)
        {
            if(Input.GetAxis("Horizontal")>0)
            controller.transform.localEulerAngles = new Vector3(0, 90, 0);
            else
                controller.transform.localEulerAngles = new Vector3(0, -90, 0);
        }
        anim.SetFloat("Speed", Mathf.Abs(Input.GetAxis("Horizontal")));
        
        controller.Move(transform.forward * forwardSpeed * Time.deltaTime* Mathf.Abs(Input.GetAxis("Horizontal")));
    }

    public void MoveTo(GameObject toPoint)
    {
        controller.enabled = false;
        controller.transform.position = toPoint.transform.position;
        controller.transform.localEulerAngles = toPoint.transform.localEulerAngles;
        controller.enabled = true;
    }
}
