using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{

    public GameObject teleportPoint;
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("other " + other.gameObject.name);
        if (other.tag.Contains("Player"))
        {
            other.GetComponent<PlayerController>().MoveTo(teleportPoint);
        
        }    
    }
}
